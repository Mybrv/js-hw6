//1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з
// урахуванням знижки. Викличте цей метод та результат виведіть в консоль.

const product = {
    name: "IPhone",
    price: 1000,
    discount: 20,
    finalPrice: function () {
        const discountPrice = product.price - product.price * (product.discount / 100)
        return discountPrice
    } }


console.log(product.finalPrice())

// 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
// наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
// за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи). Результат виклику
// функції виведіть з допомогою alert.

const userName = prompt("Enter name")
const userAge = +prompt("Enter you age")

const userData = {
    name: userName,
    age: userAge,
}
function greeting (obj) {
    alert(`Привіт, мене звати ${obj.name}, мені ${obj.age}`)
}

greeting(userData)

// 3. Завдання:
// Реалізувати повне клонування об'єкта.
// Технічні вимоги:
// - Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня
// вкладеність властивостей об'єкта може бути досить великою).
// - Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
// - У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.

function objCopy(obj) {
    if (obj !== 'object' || obj === null) {
        return obj;
    }
    if (Array.isArray(obj)) {
        let arrCopy = [];
        for (let i = 0; i < obj.length; i++) {
            arrCopy[i] = objCopy(obj[i]);
        }
        return arrCopy
    }
    let copyObj = {};
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            copyObj[key] = objCopy(obj[key]);
        }
    }
    return copyObj
}

let sourceObj = {
    name: "Vasya",
    age: 25,
    pets: {
        cats: 1,
        dog: 2,
        admin: 3
    },
    hobby: ["football", ["premirleague", "fifa2020", "NHL"], "vr", "crypto", "gym"]
}

let copy = objCopy(sourceObj)

console.log(copy)